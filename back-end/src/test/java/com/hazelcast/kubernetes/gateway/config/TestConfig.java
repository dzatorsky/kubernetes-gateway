package com.hazelcast.kubernetes.gateway.config;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class TestConfig {

    @Bean(destroyMethod = "after", initMethod = "before")
    public KubernetesServer testKubernetesServer() {
        return new KubernetesServer(true, true);
    }

    @Primary
    @Bean(destroyMethod = "close")
    public KubernetesClient testKubernetesClient(KubernetesServer kubernetesServer) {
        return kubernetesServer.getClient();
    }
}
