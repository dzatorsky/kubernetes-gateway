package com.hazelcast.kubernetes.gateway.client.impl;

import com.hazelcast.kubernetes.gateway.dto.DeploymentDTO;
import com.hazelcast.kubernetes.gateway.dto.ImageDTO;
import com.hazelcast.kubernetes.gateway.exceptions.KubernetesApiException;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.apps.DeploymentList;
import io.fabric8.kubernetes.api.model.apps.DoneableDeployment;
import io.fabric8.kubernetes.client.AppsAPIGroupClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.RollableScalableResource;
import io.fabric8.kubernetes.client.dsl.base.BaseOperation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class DeploymentApiClientImplTest {

    private static final String TEST_IMAGE_NAME = "test-image-name";
    private static final String TEST_NAMESPACE = "test-namespace";
    private static final String TEST_DEPLOYMENT_UUID = "test-deployment-uuid";
    private static final String TEST_DEPLOYMENT_NAME = "test-deployment-name";

    @Mock
    private KubernetesClient kubernetesClient;

    @Mock
    private AppsAPIGroupClient appsAPIGroupClient;

    @Mock
    private RollableScalableResource<Deployment, DoneableDeployment> deploymentByNameOperation;

    @Mock
    private BaseOperation<Deployment, DeploymentList, DoneableDeployment, RollableScalableResource<Deployment, DoneableDeployment>> deploymentListOperation;

    @Mock
    private BaseOperation<Deployment, DeploymentList, DoneableDeployment, RollableScalableResource<Deployment, DoneableDeployment>> deploymentListOperationWithNS;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Deployment deployment;

    private DeploymentApiClientImpl target;

    @BeforeEach
    private void setUp() {
        MockitoAnnotations.initMocks(this);

        mockDeployment();
        mockKubernetesClient();

        target = new DeploymentApiClientImpl(kubernetesClient);
    }

    @Test
    void testGetDeployments() {
        List<DeploymentDTO> actualDeployments = target.getDeployments(TEST_NAMESPACE);

        assertThat(actualDeployments, hasSize(1));
        actualDeployments.forEach(actualDeployment -> assertEquals(getExpectedDeploymentDTO(), actualDeployment));
    }

    @Test
    void testGetDeploymentsWhenThereIsNoEntries() {
        when(deploymentListOperationWithNS.list()).thenReturn(new DeploymentList());

        List<DeploymentDTO> actualDeployments = target.getDeployments(TEST_NAMESPACE);

        assertThat(actualDeployments, hasSize(0));
    }

    @Test
    void testGetDeploymentsOnApiFailure() {
        when(deploymentListOperationWithNS.list()).thenThrow(new RuntimeException("Network error"));

        KubernetesApiException exception = assertThrows(KubernetesApiException.class, () -> target.getDeployments(TEST_NAMESPACE));

        assertEquals("Could not get k8s deployments for namespace 'test-namespace'", exception.getReason());
    }

    @Test
    void testGetDeploymentByName() {
        DeploymentDTO actualDeployment = target.getDeploymentByName(TEST_DEPLOYMENT_NAME, TEST_NAMESPACE).get();

        assertEquals(getExpectedDeploymentDTO(), actualDeployment);
    }

    @Test
    void testGetNonExistingDeploymentByName() {
        when(deploymentByNameOperation.get()).thenReturn(null);

        boolean present = target.getDeploymentByName(TEST_DEPLOYMENT_NAME, TEST_NAMESPACE).isPresent();

        assertFalse("Deployment should not be found since it doesn't exist", present);
    }

    private void mockKubernetesClient() {
        when(kubernetesClient.apps()).thenReturn(appsAPIGroupClient);
        when(appsAPIGroupClient.deployments()).thenReturn(deploymentListOperation);
        when(deploymentListOperation.inNamespace(TEST_NAMESPACE)).thenReturn(deploymentListOperationWithNS);

        DeploymentList deploymentList = new DeploymentList();

        List<Deployment> deployments = new ArrayList<>();
        deployments.add(deployment);

        deploymentList.setItems(deployments);

        when(deploymentListOperationWithNS.list()).thenReturn(deploymentList);
        when(deploymentListOperationWithNS.withName(TEST_DEPLOYMENT_NAME)).thenReturn(deploymentByNameOperation);

        when(deploymentByNameOperation.get()).thenReturn(deployment);
    }

    private void mockDeployment() {
        when(deployment
                .getSpec()
                .getTemplate()
                .getSpec()
                .getContainers()).thenReturn(getContainers());

        when(deployment.getMetadata()).thenReturn(getMetadata());
    }

    private List<Container> getContainers() {
        Container container = new Container();
        container.setImage(TEST_IMAGE_NAME);

        List<Container> containers = new ArrayList<>();
        containers.add(container);
        return containers;
    }

    private ObjectMeta getMetadata() {
        ObjectMeta metadata = new ObjectMeta();
        metadata.setUid(TEST_DEPLOYMENT_UUID);
        metadata.setName(TEST_DEPLOYMENT_NAME);
        metadata.setNamespace(TEST_NAMESPACE);
        return metadata;
    }

    private DeploymentDTO getExpectedDeploymentDTO() {
        DeploymentDTO dto = new DeploymentDTO();

        dto.setUid(TEST_DEPLOYMENT_UUID);
        dto.setName(TEST_DEPLOYMENT_NAME);
        dto.setNamespace(TEST_NAMESPACE);

        List<ImageDTO> images = new ArrayList<>();
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setName(TEST_IMAGE_NAME);
        images.add(imageDTO);

        dto.setImages(images);

        return dto;
    }

}
