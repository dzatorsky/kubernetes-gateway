package com.hazelcast.kubernetes.gateway.it;

import com.hazelcast.kubernetes.gateway.config.TestConfig;
import com.hazelcast.kubernetes.gateway.dto.ApiErrorDTO;
import com.hazelcast.kubernetes.gateway.dto.ListWrapperDTO;
import com.hazelcast.kubernetes.gateway.dto.Manifest;
import com.hazelcast.kubernetes.gateway.dto.ResourceDTO;
import com.hazelcast.kubernetes.gateway.repository.DeploymentRepository;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Uses embedded kubernetes server configured in {@link TestConfig#testKubernetesServer()}
 * Covers very high level use cases. All the tricky cases are covered by unit tests.
 * Uses in-memory DB for testing
 */
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class DeploymentComponentTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DeploymentRepository deploymentRepository;

    @BeforeEach
    void setUp() {
        deploymentRepository.deleteAll();
    }

    @Test
    void createAndGetDeployments() throws Exception {
        ResponseEntity<ListWrapperDTO<ResourceDTO>> createDeploymentResponse = createDeployment();
        assertEquals(HttpStatus.CREATED, createDeploymentResponse.getStatusCode());
        assertSuccessfulResponse(createDeploymentResponse);

        ResponseEntity<ListWrapperDTO<ResourceDTO>> getDeploymentsResponse = restTemplate.exchange(
                getDeploymentsUrl(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ListWrapperDTO<ResourceDTO>>() {
                }
        );
        assertEquals(HttpStatus.OK, getDeploymentsResponse.getStatusCode());
        assertSuccessfulResponse(getDeploymentsResponse);
    }

    @Test
    void createDeploymentAndGetDeploymentHistory() throws Exception {
        ResponseEntity<ListWrapperDTO<ResourceDTO>> createDeploymentResponse = createDeployment();
        assertEquals(HttpStatus.CREATED, createDeploymentResponse.getStatusCode());
        assertSuccessfulResponse(createDeploymentResponse);

        ResponseEntity<ListWrapperDTO<ResourceDTO>> getDeploymentsHistoryResponse = restTemplate.exchange(
                getHistoryURL(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ListWrapperDTO<ResourceDTO>>() {
                }
        );
        assertEquals(HttpStatus.OK, getDeploymentsHistoryResponse.getStatusCode());
        assertSuccessfulResponse(getDeploymentsHistoryResponse);
    }

    @Test
    void createPodInsteadOfExpectedDeployment() throws Exception {
        String fileContent = getFileContent("manifests/ValidPod.yaml");

        ResponseEntity<ApiErrorDTO> response = createDeployment(fileContent, new ParameterizedTypeReference<ApiErrorDTO>() {
        });

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        ApiErrorDTO errorDTO = response.getBody();
        assertEquals("Invalid manifest kind. Expected 'Deployment' but received 'Pod'.", errorDTO.getErrorMessage());
    }

    @Test
    void createDeploymentWhenManifestIsInvalid() throws Exception {
        ResponseEntity<ApiErrorDTO> response = createDeployment("invalid manifest file", new ParameterizedTypeReference<ApiErrorDTO>() {
        });

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        ApiErrorDTO errorDTO = response.getBody();
        assertEquals("Kubernetes manifest is invalid.", errorDTO.getErrorMessage());
    }

    private <T> ResponseEntity<T> createDeployment(String fileContent, ParameterizedTypeReference<T> typeReference) throws URISyntaxException {
        Manifest manifest = new Manifest();
        manifest.setContent(fileContent);

        return restTemplate.exchange(
                getDeploymentsUrl(),
                HttpMethod.POST,
                new HttpEntity<>(manifest),
                typeReference
        );
    }

    private void assertSuccessfulResponse(ResponseEntity<ListWrapperDTO<ResourceDTO>> response) {

        List<ResourceDTO> items = response.getBody().getItems();
        assertThat(items, hasSize(1));

        items.forEach(createdResource -> {
            assertEquals("nginx-deployment", createdResource.getName());
            assertEquals("test-namespace", createdResource.getNamespace());
        });
    }

    private ResponseEntity<ListWrapperDTO<ResourceDTO>> createDeployment() throws IOException {
        String fileContent = getFileContent("manifests/ValidDeployment.yaml");

        Manifest manifest = new Manifest();
        manifest.setContent(fileContent);

        return restTemplate.exchange(
                getDeploymentsUrl(),
                HttpMethod.POST,
                new HttpEntity<>(manifest),
                new ParameterizedTypeReference<ListWrapperDTO<ResourceDTO>>() {
                }
        );
    }

    @SneakyThrows
    private URI getDeploymentsUrl() {
        return new URI("http://localhost:" + port + "/api/v1/gateway/namespaces/test-namespace/deployments");
    }

    @SneakyThrows
    private URI getHistoryURL() {
        return new URI("http://localhost:" + port + "/api/v1/history/deployments");
    }

    private String getFileContent(String filePath) throws IOException {
        InputStream manifestStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath);
        return IOUtils.toString(manifestStream, Charset.defaultCharset());
    }

}

