package com.hazelcast.kubernetes.gateway.client.impl;

import com.hazelcast.kubernetes.gateway.dto.ResourceDTO;
import com.hazelcast.kubernetes.gateway.exceptions.InvalidManifestKindException;
import com.hazelcast.kubernetes.gateway.exceptions.InvalidManifestException;
import com.hazelcast.kubernetes.gateway.exceptions.KubernetesApiException;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.NamespaceVisitFromServerGetWatchDeleteRecreateWaitApplicable;
import io.fabric8.kubernetes.client.dsl.ParameterNamespaceListVisitFromServerGetDeleteRecreateWaitApplicable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ResourcesApiClientImplTest {

    private static final String TEST_NAMESPACE = "test-namespace";
    private static final String TEST_DEPLOYMENT_UUID = "test-deployment-uuid";
    private static final String TEST_DEPLOYMENT_NAME = "test-deployment-name";
    private static final String TEST_MANIFEST_FILE = "test-manifest-file";
    private static final String REQUIRED_KIND = "Deployment";

    @Mock
    private ParameterNamespaceListVisitFromServerGetDeleteRecreateWaitApplicable<HasMetadata, Boolean> loadedData;

    @Mock
    private NamespaceVisitFromServerGetWatchDeleteRecreateWaitApplicable<HasMetadata, Boolean> createResourceOperation;

    @Mock
    private NamespaceVisitFromServerGetWatchDeleteRecreateWaitApplicable<HasMetadata, Boolean> createResourceOperationWithNS;

    @Mock
    private KubernetesClient kubernetesClient;

    private ResourcesApiClientImpl target;

    private List<HasMetadata> parsedManifest;

    @BeforeEach
    private void setUp() {
        MockitoAnnotations.initMocks(this);

        when(kubernetesClient.load(any(InputStream.class))).thenReturn(loadedData);

        parsedManifest = new ArrayList<>();
        parsedManifest.add(new Deployment());
        when(loadedData.get()).thenReturn(parsedManifest);

        when(kubernetesClient.resource(any(HasMetadata.class))).thenReturn(createResourceOperation);
        when(createResourceOperation.inNamespace(TEST_NAMESPACE)).thenReturn(createResourceOperationWithNS);

        Deployment createdResource = new Deployment();
        createdResource.setMetadata(getMetadata());
        when(createResourceOperationWithNS.createOrReplace()).thenReturn(createdResource);

        target = new ResourcesApiClientImpl(kubernetesClient);
    }

    @Test
    void testCreateResources() {
        List<ResourceDTO> createdResources = target.createResources(TEST_NAMESPACE, TEST_MANIFEST_FILE, REQUIRED_KIND);

        assertThat(createdResources, hasSize(1));
        createdResources.forEach(actualDeployment -> assertEquals(getExpectedResourceDTO(), actualDeployment));
    }

    @Test
    void testCreateResourcesWhenManifestIsInvalid() {
        when(kubernetesClient.load(any(InputStream.class))).thenThrow(new RuntimeException("Manifest is incorrect"));

        InvalidManifestException exception = assertThrows(InvalidManifestException.class, () -> target.createResources(TEST_NAMESPACE, TEST_MANIFEST_FILE, REQUIRED_KIND));

        assertEquals("Kubernetes manifest is invalid.", exception.getReason());
    }

    @Test
    void testCreateResourcesWhenInvalidKindIsRequested() {
        parsedManifest.clear();
        parsedManifest.add(new Pod());

        InvalidManifestKindException exception = assertThrows(InvalidManifestKindException.class, () -> target.createResources(TEST_NAMESPACE, TEST_MANIFEST_FILE, REQUIRED_KIND));

        assertEquals("Invalid manifest kind. Expected 'Deployment' but received 'Pod'.", exception.getReason());
    }

    @Test
    void testCreateResourcesWhenValidAndInvalidKindsAreRequested() {
        parsedManifest.clear();
        parsedManifest.add(new Deployment());
        parsedManifest.add(new Pod());

        InvalidManifestKindException exception = assertThrows(InvalidManifestKindException.class, () -> target.createResources(TEST_NAMESPACE, TEST_MANIFEST_FILE, REQUIRED_KIND));

        assertEquals("Invalid manifest kind. Expected 'Deployment' but received 'Pod'.", exception.getReason());
    }

    @Test
    void testCreateResourcesWhenApiCallFailed() {
        when(createResourceOperationWithNS.createOrReplace()).thenThrow(new RuntimeException("API request failed"));

        KubernetesApiException exception = assertThrows(KubernetesApiException.class, () -> target.createResources(TEST_NAMESPACE, TEST_MANIFEST_FILE, REQUIRED_KIND));

        assertEquals("Could not create resources for namespace 'test-namespace' ", exception.getReason());
    }

    private ObjectMeta getMetadata() {
        ObjectMeta metadata = new ObjectMeta();
        metadata.setUid(TEST_DEPLOYMENT_UUID);
        metadata.setName(TEST_DEPLOYMENT_NAME);
        metadata.setNamespace(TEST_NAMESPACE);
        return metadata;
    }

    private ResourceDTO getExpectedResourceDTO() {
        ResourceDTO dto = new ResourceDTO();

        dto.setUid(TEST_DEPLOYMENT_UUID);
        dto.setName(TEST_DEPLOYMENT_NAME);
        dto.setNamespace(TEST_NAMESPACE);

        return dto;
    }

}
