package com.hazelcast.kubernetes.gateway.controller;

import com.hazelcast.kubernetes.gateway.dto.DeploymentDTO;
import com.hazelcast.kubernetes.gateway.dto.ImageDTO;
import com.hazelcast.kubernetes.gateway.dto.Manifest;
import com.hazelcast.kubernetes.gateway.service.DeploymentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DeploymentController.class)
@TestPropertySource(properties = "keycloak.enabled=false")
public class DeploymentControllerTest {

    private static final String TEST_IMAGE_NAME = "test-image-name";
    private static final String TEST_NAMESPACE = "test-namespace";
    private static final String TEST_DEPLOYMENT_UUID = "test-deployment-uuid";
    private static final String TEST_DEPLOYMENT_NAME = "test-deployment-name";
    private static final String TEST_MANIFEST = "test-manifest";
    private static final String DEPLOYMENTS_PATH = "/gateway/namespaces/test-namespace/deployments";
    private static final String SELF_REF = "http://localhost" + DEPLOYMENTS_PATH;
    private static final String DEPLOYMENT_HISTORY_PATH = "/history/deployments";
    private static final String DEPLOYMENT_HISTORY_SELF_REF = "http://localhost" + DEPLOYMENT_HISTORY_PATH;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeploymentService deploymentService;

    @BeforeEach
    void setUp() {
        when(deploymentService.getDeployments(TEST_NAMESPACE)).thenReturn(Collections.singletonList(getDeploymentDTO()));
        when(deploymentService.getDeploymentsHistory()).thenReturn(Collections.singletonList(getDeploymentDTO()));
        when(deploymentService.createDeployments(TEST_NAMESPACE, TEST_MANIFEST)).thenReturn(Collections.singletonList(getDeploymentDTO()));
    }

    @Test
    void getDeployments() throws Exception {
        mvc.perform(get(DEPLOYMENTS_PATH)
                .accept(MediaTypes.HAL_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.items[0].uid").value(TEST_DEPLOYMENT_UUID))
                .andExpect(jsonPath("$.items[0].name").value(TEST_DEPLOYMENT_NAME))
                .andExpect(jsonPath("$.items[0].namespace").value(TEST_NAMESPACE))
                .andExpect(jsonPath("$.items[0].images[0].name").value(TEST_IMAGE_NAME))

                .andExpect(jsonPath("$._links.self.href").value(SELF_REF));
    }

    @Test
    void getDeploymentsWhenEmptyList() throws Exception {
        when(deploymentService.getDeployments(TEST_NAMESPACE)).thenReturn(new ArrayList<>());

        mvc.perform(get(DEPLOYMENTS_PATH)
                .accept(MediaTypes.HAL_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.items").isEmpty())

                .andExpect(jsonPath("$._links.self.href").value(SELF_REF));
    }

    @Test
    void getDeploymentHistory() throws Exception {
        mvc.perform(get(DEPLOYMENT_HISTORY_PATH)
                .accept(MediaTypes.HAL_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.items[0].uid").value(TEST_DEPLOYMENT_UUID))
                .andExpect(jsonPath("$.items[0].name").value(TEST_DEPLOYMENT_NAME))
                .andExpect(jsonPath("$.items[0].namespace").value(TEST_NAMESPACE))
                .andExpect(jsonPath("$.items[0].images[0].name").value(TEST_IMAGE_NAME))

                .andExpect(jsonPath("$._links.self.href").value(DEPLOYMENT_HISTORY_SELF_REF));
    }

    @Test
    void getDeploymentHistoryWhenEmptyList() throws Exception {
        when(deploymentService.getDeploymentsHistory()).thenReturn(new ArrayList<>());

        mvc.perform(get(DEPLOYMENT_HISTORY_PATH)
                .accept(MediaTypes.HAL_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.items").isEmpty())

                .andExpect(jsonPath("$._links.self.href").value(DEPLOYMENT_HISTORY_SELF_REF));
    }

    @Test
    void createDeployments() throws Exception {
        Manifest manifest = new Manifest();
        manifest.setContent(TEST_MANIFEST);

        mvc.perform(post(DEPLOYMENTS_PATH)
                .content("{ \"content\" : \"" + TEST_MANIFEST + "\" }")
                .contentType(APPLICATION_JSON_VALUE)
                .accept(MediaTypes.HAL_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.items[0].uid").value(TEST_DEPLOYMENT_UUID))
                .andExpect(jsonPath("$.items[0].name").value(TEST_DEPLOYMENT_NAME))
                .andExpect(jsonPath("$.items[0].namespace").value(TEST_NAMESPACE))
                .andExpect(jsonPath("$.items[0].images[0].name").value(TEST_IMAGE_NAME))

                .andExpect(jsonPath("$._links.self.href").value(SELF_REF));
    }

    private DeploymentDTO getDeploymentDTO() {
        DeploymentDTO dto = new DeploymentDTO();

        dto.setUid(TEST_DEPLOYMENT_UUID);
        dto.setName(TEST_DEPLOYMENT_NAME);
        dto.setNamespace(TEST_NAMESPACE);

        List<ImageDTO> images = new ArrayList<>();
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setName(TEST_IMAGE_NAME);
        images.add(imageDTO);

        dto.setImages(images);

        return dto;
    }
}
