package com.hazelcast.kubernetes.gateway.controller.handler;

import com.hazelcast.kubernetes.gateway.controller.DeploymentController;
import com.hazelcast.kubernetes.gateway.service.DeploymentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Testing exception handling. As a reference we used {@link DeploymentController} for testing.
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(DeploymentController.class)
@Import(ApiExceptionHandler.class)
@TestPropertySource(properties = "keycloak.enabled=false")
public class ApiExceptionHandlerTest {

    private static final String TEST_NAMESPACE = "test-namespace";
    private static final String TEST_MESSAGE = "Test message";
    private static final String CAUSE_MESSAGE = "Cause exception message";
    private static final String TEST_PATH = "/gateway/namespaces/test-namespace/deployments";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeploymentService deploymentService;

    @Test
    void testHandleResponseStatusException() throws Exception {
        when(deploymentService.getDeployments(TEST_NAMESPACE)).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, TEST_MESSAGE, new RuntimeException(CAUSE_MESSAGE)));

        mvc.perform(get(TEST_PATH)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.errorMessage").value(TEST_MESSAGE))
                .andExpect(jsonPath("$.cause").value(CAUSE_MESSAGE));
    }

    @Test
    void testHandleRuntimeException() throws Exception {
        when(deploymentService.getDeployments(TEST_NAMESPACE)).thenThrow(new RuntimeException(TEST_MESSAGE));

        mvc.perform(get(TEST_PATH)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isInternalServerError())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.errorMessage").value(TEST_MESSAGE))
                .andExpect(jsonPath("$.cause").isEmpty());
    }

    @Test
    void testHandleCommonExceptionProcessedBySpringAutomatically() throws Exception {
        mvc.perform(post(TEST_PATH)
                .content("invalid data")
                .accept(MediaType.APPLICATION_OCTET_STREAM))
                .andDo(print())
                .andExpect(status().isUnsupportedMediaType());
    }
}
