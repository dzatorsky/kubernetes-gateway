package com.hazelcast.kubernetes.gateway.controller.handler;

import com.hazelcast.kubernetes.gateway.dto.ApiErrorDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static java.util.Optional.ofNullable;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ApiErrorDTO> handleBadRequest(ResponseStatusException ex) {

        ApiErrorDTO dto = getApiErrorDTO(ex, ex.getReason());

        return new ResponseEntity<>(dto, new HttpHeaders(), ex.getStatus());
    }

    @ExceptionHandler
    public ResponseEntity<ApiErrorDTO> handleUnexpectedException(Throwable throwable) {

        ApiErrorDTO dto = getApiErrorDTO(throwable, throwable.getMessage());

        return new ResponseEntity<>(dto, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ApiErrorDTO getApiErrorDTO(Throwable throwable, String reason) {
        ApiErrorDTO dto = new ApiErrorDTO();
        dto.setErrorMessage(reason);
        dto.setCause(getCause(throwable));
        return dto;
    }

    private String getCause(Throwable throwable) {
        return ofNullable(throwable.getCause()).map(Throwable::getMessage).orElse(null);
    }

}
