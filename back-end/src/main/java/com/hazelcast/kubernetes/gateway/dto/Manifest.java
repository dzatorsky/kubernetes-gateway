package com.hazelcast.kubernetes.gateway.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Manifest {

    @NotNull
    private String content;

}
