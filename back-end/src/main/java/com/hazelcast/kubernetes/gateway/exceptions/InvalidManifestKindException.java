package com.hazelcast.kubernetes.gateway.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidManifestKindException extends ResponseStatusException {
    public InvalidManifestKindException(String expectedKind, String actualKind) {
        super(HttpStatus.BAD_REQUEST, "Invalid manifest kind. Expected '" + expectedKind + "' but received '" + actualKind + "'.");
    }
}
