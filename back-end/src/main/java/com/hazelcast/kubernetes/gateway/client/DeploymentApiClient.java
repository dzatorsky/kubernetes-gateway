package com.hazelcast.kubernetes.gateway.client;

import com.hazelcast.kubernetes.gateway.dto.DeploymentDTO;

import java.util.List;
import java.util.Optional;

public interface DeploymentApiClient {
    List<DeploymentDTO> getDeployments(String namespace);

    Optional<DeploymentDTO> getDeploymentByName(String name, String namespace);
}
