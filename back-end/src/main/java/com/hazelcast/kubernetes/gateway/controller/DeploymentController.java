package com.hazelcast.kubernetes.gateway.controller;

import com.hazelcast.kubernetes.gateway.dto.DeploymentDTO;
import com.hazelcast.kubernetes.gateway.dto.ListWrapperDTO;
import com.hazelcast.kubernetes.gateway.dto.Manifest;
import com.hazelcast.kubernetes.gateway.service.DeploymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequiredArgsConstructor
public class DeploymentController {

    private final DeploymentService deploymentService;

    @GetMapping("/history/deployments")
    public ResponseEntity<ListWrapperDTO<DeploymentDTO>> getDeploymentsHistory() {
        List<DeploymentDTO> deployments = deploymentService.getDeploymentsHistory();
        Link link = linkTo(methodOn(DeploymentController.class).getDeploymentsHistory()).withSelfRel();

        return getResponseEntity(deployments, link);
    }

    @GetMapping("/gateway/namespaces/{namespace}/deployments")
    public ResponseEntity<ListWrapperDTO<DeploymentDTO>> getDeployments(@PathVariable("namespace") String namespace) {
        List<DeploymentDTO> deployments = deploymentService.getDeployments(namespace);
        Link link = linkTo(methodOn(DeploymentController.class).getDeployments(namespace)).withSelfRel();

        return getResponseEntity(deployments, link);
    }

    @PostMapping("/gateway/namespaces/{namespace}/deployments")
    public ResponseEntity<ListWrapperDTO<DeploymentDTO>> createDeployment(@PathVariable("namespace") String namespace, @RequestBody @Valid Manifest manifest) {
        List<DeploymentDTO> createdDeployments = deploymentService.createDeployments(namespace, manifest.getContent());

        ListWrapperDTO<DeploymentDTO> wrapper = new ListWrapperDTO<>(createdDeployments);

        Link link = linkTo(methodOn(DeploymentController.class).createDeployment(namespace, null)).withSelfRel();
        wrapper.add(link);

        return new ResponseEntity<>(wrapper, HttpStatus.CREATED);
    }

    private ResponseEntity<ListWrapperDTO<DeploymentDTO>> getResponseEntity(List<DeploymentDTO> deployments, Link link) {
        ListWrapperDTO<DeploymentDTO> wrapper = new ListWrapperDTO<>(deployments);
        wrapper.add(link);

        if (wrapper.getItems().isEmpty()) {
            return new ResponseEntity<>(wrapper, HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(wrapper, HttpStatus.OK);
        }
    }


}
