package com.hazelcast.kubernetes.gateway.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ResourceDTO {
    private Long id;

    private String uid;
    private String name;

    private String namespace;

    private LocalDateTime timestamp;
}
