package com.hazelcast.kubernetes.gateway.dto;

import lombok.Data;

@Data
public class ApiErrorDTO {
    private String errorMessage;
    private String cause;
}
