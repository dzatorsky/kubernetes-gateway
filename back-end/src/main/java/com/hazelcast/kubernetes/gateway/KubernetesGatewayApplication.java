package com.hazelcast.kubernetes.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KubernetesGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(KubernetesGatewayApplication.class, args);
    }

}
