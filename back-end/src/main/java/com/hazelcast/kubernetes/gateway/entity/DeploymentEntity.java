package com.hazelcast.kubernetes.gateway.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="deployment")
public class DeploymentEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String uid;
    private String name;

    private String namespace;

    private LocalDateTime timestamp;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "image_id")
    private List<ImageEntity> images = new ArrayList<>();

}
