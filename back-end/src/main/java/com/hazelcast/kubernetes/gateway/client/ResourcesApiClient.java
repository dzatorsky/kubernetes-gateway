package com.hazelcast.kubernetes.gateway.client;

import com.hazelcast.kubernetes.gateway.dto.ResourceDTO;

import java.util.List;

public interface ResourcesApiClient {
    List<ResourceDTO> createResources(String namespace, String yamlDefinition, String requiredKind);
}
