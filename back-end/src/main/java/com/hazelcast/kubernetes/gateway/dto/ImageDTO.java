package com.hazelcast.kubernetes.gateway.dto;

import lombok.Data;

@Data
public class ImageDTO {
    private Long id;
    private String name;
}
