package com.hazelcast.kubernetes.gateway.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class DeploymentDTO extends ResourceDTO {
    private List<ImageDTO> images = new ArrayList<>();
}

