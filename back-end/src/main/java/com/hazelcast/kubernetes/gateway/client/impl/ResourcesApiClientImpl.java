package com.hazelcast.kubernetes.gateway.client.impl;

import com.hazelcast.kubernetes.gateway.client.ResourcesApiClient;
import com.hazelcast.kubernetes.gateway.dto.ResourceDTO;
import com.hazelcast.kubernetes.gateway.exceptions.InvalidManifestException;
import com.hazelcast.kubernetes.gateway.exceptions.InvalidManifestKindException;
import com.hazelcast.kubernetes.gateway.exceptions.KubernetesApiException;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.client.KubernetesClient;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ResourcesApiClientImpl implements ResourcesApiClient {

    private final KubernetesClient kubernetesClient;

    @Override
    public List<ResourceDTO> createResources(String namespace, String yamlManifest, String requiredKind) {

        List<HasMetadata> resources = parseManifest(yamlManifest);

        if (requiredKind != null) {
            validateKind(requiredKind, resources);
        }

        return resources.stream()
                .map(resource -> createOrReplaceResource(namespace, resource))
                .map(this::toResourceDTO)
                .collect(Collectors.toList());
    }

    private List<HasMetadata> parseManifest(String yamlDefinition) {
        List<HasMetadata> resources;

        try (InputStream definitionInputStream = IOUtils.toInputStream(yamlDefinition, Charset.defaultCharset())) {
            resources = kubernetesClient.load(definitionInputStream).get();
        } catch (Exception e) {
            throw new InvalidManifestException("Kubernetes manifest is invalid.", e);
        }

        return resources;
    }

    private void validateKind(String requiredKind, List<HasMetadata> resources) {
        resources.stream()
                .filter(resource -> !requiredKind.equalsIgnoreCase(resource.getKind()))
                .findFirst()
                .ifPresent(resource -> {
                    throw new InvalidManifestKindException(requiredKind, resource.getKind());
                });
    }

    private HasMetadata createOrReplaceResource(String namespace, HasMetadata resource) {
        try {
            return kubernetesClient.resource(resource).inNamespace(namespace).createOrReplace();
        } catch (Exception e) {
            throw new KubernetesApiException("Could not create resources for namespace '" + namespace + "' ", e);
        }
    }

    private ResourceDTO toResourceDTO(HasMetadata hasMetadata) {
        ObjectMeta metadata = hasMetadata.getMetadata();

        ResourceDTO resourceDTO = new ResourceDTO();
        resourceDTO.setUid(metadata.getUid());
        resourceDTO.setName(metadata.getName());
        resourceDTO.setNamespace(metadata.getNamespace());

        return resourceDTO;
    }
}
