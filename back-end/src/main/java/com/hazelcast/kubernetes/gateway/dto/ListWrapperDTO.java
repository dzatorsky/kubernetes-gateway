package com.hazelcast.kubernetes.gateway.dto;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

@Data
public class ListWrapperDTO<T> extends ResourceSupport {

    private final List<T> items;

    public ListWrapperDTO() {
        this.items = new ArrayList<>();
    }

    public ListWrapperDTO(List<T> items) {
        this.items = items;
    }
}
