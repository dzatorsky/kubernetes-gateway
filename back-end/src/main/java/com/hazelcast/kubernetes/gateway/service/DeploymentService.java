package com.hazelcast.kubernetes.gateway.service;

import com.hazelcast.kubernetes.gateway.client.DeploymentApiClient;
import com.hazelcast.kubernetes.gateway.client.ResourcesApiClient;
import com.hazelcast.kubernetes.gateway.dto.DeploymentDTO;
import com.hazelcast.kubernetes.gateway.entity.DeploymentEntity;
import com.hazelcast.kubernetes.gateway.mapper.DeploymentMapper;
import com.hazelcast.kubernetes.gateway.repository.DeploymentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class DeploymentService {

    private static final String DEPLOYMENT_KIND = "Deployment";

    private final DeploymentMapper deploymentMapper;
    private final ResourcesApiClient resourcesApiClient;
    private final DeploymentApiClient deploymentApiClient;
    private final DeploymentRepository deploymentRepository;

    public List<DeploymentDTO> getDeploymentsHistory() {
        return deploymentRepository.findAll()
                .stream()
                .map(deploymentMapper::toDTO)
                .collect(Collectors.toList());
    }

    public List<DeploymentDTO> getDeployments(String namespace) {
        return deploymentApiClient.getDeployments(namespace);
    }

    public List<DeploymentDTO> createDeployments(String namespace, String yamlDefinition) {
        return resourcesApiClient.createResources(namespace, yamlDefinition, DEPLOYMENT_KIND)
                .stream()
                .map(resourceDTO -> {
                    DeploymentDTO dto = deploymentApiClient.getDeploymentByName(resourceDTO.getName(), namespace).orElseGet(() -> deploymentMapper.toDTO(resourceDTO));

                    DeploymentEntity deploymentEntity = deploymentRepository.save(deploymentMapper.toEntity(dto));
                    dto.setId(deploymentEntity.getId());
                    dto.setTimestamp(deploymentEntity.getTimestamp());

                    return dto;
                })
                .collect(toList());
    }
}
