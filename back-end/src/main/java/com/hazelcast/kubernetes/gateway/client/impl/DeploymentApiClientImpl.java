package com.hazelcast.kubernetes.gateway.client.impl;

import com.hazelcast.kubernetes.gateway.client.DeploymentApiClient;
import com.hazelcast.kubernetes.gateway.dto.DeploymentDTO;
import com.hazelcast.kubernetes.gateway.dto.ImageDTO;
import com.hazelcast.kubernetes.gateway.exceptions.KubernetesApiException;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PodSpec;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.apps.DeploymentList;
import io.fabric8.kubernetes.api.model.apps.DeploymentSpec;
import io.fabric8.kubernetes.api.model.apps.DoneableDeployment;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.AppsAPIGroupDSL;
import io.fabric8.kubernetes.client.dsl.Gettable;
import io.fabric8.kubernetes.client.dsl.NonNamespaceOperation;
import io.fabric8.kubernetes.client.dsl.RollableScalableResource;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class DeploymentApiClientImpl implements DeploymentApiClient {

    private final KubernetesClient kubernetesClient;

    @Override
    public List<DeploymentDTO> getDeployments(String namespace) {
        return ofNullable(kubernetesClient.apps())
                .map(AppsAPIGroupDSL::deployments)
                .map(deployment -> deployment.inNamespace(namespace))
                .map(operation -> getDeployments(namespace, operation))
                .map(DeploymentList::getItems)
                .flatMap(deployments -> of(mapDeployments(deployments)))
                .orElse(new ArrayList<>());
    }

    @Override
    public Optional<DeploymentDTO> getDeploymentByName(String name, String namespace) {
        return ofNullable(kubernetesClient.apps())
                .map(AppsAPIGroupDSL::deployments)
                .map(deployment -> deployment.inNamespace(namespace).withName(name))
                .map(Gettable::get)
                .map(this::mapDeployment);

    }

    private DeploymentList getDeployments(String namespace, NonNamespaceOperation<Deployment, DeploymentList, DoneableDeployment, RollableScalableResource<Deployment, DoneableDeployment>> operation) {
        try {
            return operation.list();
        } catch (Exception e) {
            throw new KubernetesApiException("Could not get k8s deployments for namespace '" + namespace + "'", e);
        }
    }

    private List<DeploymentDTO> mapDeployments(List<Deployment> deployments) {
        return deployments
                .stream()
                .map(this::mapDeployment)
                .collect(toList());
    }

    private DeploymentDTO mapDeployment(Deployment deployment) {
        List<ImageDTO> images = ofNullable(deployment)
                .map(Deployment::getSpec)
                .map(DeploymentSpec::getTemplate)
                .map(PodTemplateSpec::getSpec)
                .map(PodSpec::getContainers)
                .flatMap(containersStream -> of(mapImages(containersStream)))
                .orElse(new ArrayList<>());

        ObjectMeta metaData = ofNullable(deployment)
                .map(Deployment::getMetadata)
                .orElse(new ObjectMeta());

        return mapDeploymentDTO(images, metaData);
    }

    private List<ImageDTO> mapImages(List<Container> containers) {
        return containers
                .stream()
                .map(Container::getImage)
                .map(image -> {
                    ImageDTO imageDTO = new ImageDTO();
                    imageDTO.setName(image);

                    return imageDTO;
                })
                .collect(toList());
    }

    private DeploymentDTO mapDeploymentDTO(List<ImageDTO> images, ObjectMeta metaData) {
        DeploymentDTO deploymentDTO = new DeploymentDTO();
        deploymentDTO.setImages(images);
        deploymentDTO.setUid(metaData.getUid());
        deploymentDTO.setName(metaData.getName());
        deploymentDTO.setNamespace(metaData.getNamespace());
        return deploymentDTO;
    }
}
