package com.hazelcast.kubernetes.gateway.mapper;

import com.hazelcast.kubernetes.gateway.dto.DeploymentDTO;
import com.hazelcast.kubernetes.gateway.dto.ResourceDTO;
import com.hazelcast.kubernetes.gateway.entity.DeploymentEntity;
import org.mapstruct.Mapper;

@Mapper
public interface DeploymentMapper {
    DeploymentDTO toDTO(DeploymentEntity entity);

    DeploymentDTO toDTO(ResourceDTO dto);

    DeploymentEntity toEntity(DeploymentDTO dto);
}
