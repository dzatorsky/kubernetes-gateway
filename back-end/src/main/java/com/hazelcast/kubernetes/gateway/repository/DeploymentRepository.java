package com.hazelcast.kubernetes.gateway.repository;

import com.hazelcast.kubernetes.gateway.entity.DeploymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeploymentRepository extends JpaRepository<DeploymentEntity, Long> {
}
