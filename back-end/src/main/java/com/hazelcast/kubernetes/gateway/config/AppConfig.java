package com.hazelcast.kubernetes.gateway.config;

import io.fabric8.kubernetes.client.AutoAdaptableKubernetesClient;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.KubernetesClient;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.nio.charset.StandardCharsets;

@Configuration
public class AppConfig {

    @SneakyThrows
    @Bean(destroyMethod = "close")
    public KubernetesClient kubernetesClient(@Value("${app.kubeconfig.path}") String kubeConfigFilePath){

        File kubeconfig = new File(kubeConfigFilePath);
        String configStr = FileUtils.readFileToString(kubeconfig, StandardCharsets.UTF_8);
        Config config = Config.fromKubeconfig(configStr);

        return new AutoAdaptableKubernetesClient(config);
    }

}
