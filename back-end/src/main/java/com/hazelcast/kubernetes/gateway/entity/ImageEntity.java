package com.hazelcast.kubernetes.gateway.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name="images")
public class ImageEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
}
