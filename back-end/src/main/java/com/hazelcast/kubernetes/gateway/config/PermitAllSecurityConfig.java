package com.hazelcast.kubernetes.gateway.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Disables security in case keycloak.enabled is set to false. Useful for manual and automated testing.
 */
@Configuration
@ConditionalOnProperty(value = "keycloak.enabled", matchIfMissing = true, havingValue = "false")
class PermitAllSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http
                .csrf().disable()
                .authorizeRequests()
                .anyRequest().permitAll();
    }
}

