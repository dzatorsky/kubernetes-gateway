import {ApiErrorModel} from '../../../deployments/model/api-error.model';

export interface ErrorDataModel {
  modalHeader: string;
  okButtonLabel: string;

  apiError: ApiErrorModel;
}
