import {NgModule} from '@angular/core';
import {ErrorModalWindowComponent} from './error-modal-window.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  imports: [MatDialogModule, MatButtonModule, MatCardModule],
  exports: [ErrorModalWindowComponent],
  declarations: [ErrorModalWindowComponent],
  entryComponents: [ErrorModalWindowComponent],
})
export class ErrorModalWindowModule {
}
