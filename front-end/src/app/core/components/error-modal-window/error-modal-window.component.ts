import {Component, Inject, Input} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {ErrorDataModel} from './ErrorDataModel';

@Component({
  selector: 'ngx-error-modal-window',
  templateUrl: 'error-modal-window.component.html',
})
export class ErrorModalWindowComponent {
  constructor(private dialogRef: MatDialogRef<ErrorModalWindowComponent>, @Inject(MAT_DIALOG_DATA) private error: ErrorDataModel) {}

  close(): void {
    this.dialogRef.close();
  }
}
