import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DeploymentsRoutingModule} from './deployments-routing.module';
import {DeploymentsComponent} from './component/deployments.component';
import {MatTableModule} from '@angular/material/table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ErrorModalWindowModule} from '../core/components/error-modal-window/error-modal-window.module';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [DeploymentsComponent],
  imports: [
    CommonModule,
    DeploymentsRoutingModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    ErrorModalWindowModule,
  ],
})
export class DeploymentsModule {
}
