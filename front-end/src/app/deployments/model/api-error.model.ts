export interface ApiErrorModel {
  cause: string;
  errorMessage: string;
}
