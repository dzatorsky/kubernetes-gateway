import {ImageModel} from './image.model';

export interface DeploymentModel {
  id?: number;

  uid: string;
  name: string;
  namespace: string;
  timestamp: string;
  images : ImageModel[];
}
