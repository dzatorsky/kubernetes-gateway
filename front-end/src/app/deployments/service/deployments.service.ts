import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {map} from 'rxjs/operators';
import {ListWrapper} from '../../core/model/list-wrapper.model';
import {DeploymentModel} from '../model/deployment.model';
import {environment} from '../../../environments/environment';
import {ManifestModel} from '../model/manifest.model';

@Injectable({
  providedIn: 'root',
})
export class DeploymentsService {

  constructor(private http: HttpClient) {
  }

  getDeployments(namespace: string): Observable<ListWrapper<DeploymentModel>> {
    return this.http.get<ListWrapper<DeploymentModel>>(this.getDeploymentsURL(namespace), {observe: 'response'})
      .pipe(
        map(resp => resp.body),
      );
  }

  createDeployment(namespace: string, manifest: ManifestModel): Observable<ListWrapper<DeploymentModel>> {
    return this.http.post<ListWrapper<DeploymentModel>>(this.getDeploymentsURL(namespace), manifest, {observe: 'response'})
      .pipe(
        map(resp => resp.body),
      );
  }

  private getDeploymentsURL(namespace: string) {
    return `${environment.serverBasePath}/gateway/namespaces/${namespace}/deployments`;
  }
}
