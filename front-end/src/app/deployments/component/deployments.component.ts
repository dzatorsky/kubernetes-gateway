import {Component} from '@angular/core';
import {DeploymentsService} from '../service/deployments.service';
import {DeploymentModel} from '../model/deployment.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {ErrorModalWindowComponent} from '../../core/components/error-modal-window/error-modal-window.component';
import {ErrorDataModel} from '../../core/components/error-modal-window/ErrorDataModel';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-deployments',
  templateUrl: './deployments.component.html',
  styleUrls: ['./deployments.component.scss'],
})
export class DeploymentsComponent {

  displayedColumns: string[] = ['uid', 'name', 'namespace', 'images'];
  dataSource: DeploymentModel[] = [];

  nameSpaceFormControl = new FormControl('', [Validators.required]);

  createDeploymentForm: FormGroup = new FormGroup({
    'manifest': new FormControl('', [Validators.required]),
  }, {
    updateOn: 'submit',
  });

  constructor(private deploymentsService: DeploymentsService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  submit() {

    if (this.nameSpaceFormControl.value) {
      if (this.createDeploymentForm.valid) {
        const manifestContent = this.createDeploymentForm.get('manifest').value;

        this.deploymentsService.createDeployment(this.nameSpaceFormControl.value, {content: manifestContent})
          .subscribe(
            resp => {
              this.snackBar.open(`Deployment(s) was(were) created successfully.`, null, {
                duration: 2000,
                verticalPosition: 'top'
              });

              return this.loadDeployments();
            },
            errorResp => this.handleError(errorResp),
          );
      }
    } else {
      this.nameSpaceFormControl.markAsTouched();
    }

  }

  refreshList() {
    if (this.nameSpaceFormControl.value) {
      this.loadDeployments();
    } else {
      this.nameSpaceFormControl.markAsTouched();
    }

  }

  loadDeployments() {
    this.deploymentsService.getDeployments(this.nameSpaceFormControl.value)
      .subscribe(
        resp => this.dataSource = resp.items,
        errorResp => this.handleError(errorResp),
      )
  }


  private handleError(errorResp) {
    this.openDialog(<ErrorDataModel>{
      modalHeader: `Server responded with status: ${errorResp.status}`,
      okButtonLabel: 'Ok',
      apiError: errorResp.error,
    });
  }

  private openDialog(error: ErrorDataModel): void {
    this.dialog.open(ErrorModalWindowComponent, {
      data: error,
    });
  }

}
