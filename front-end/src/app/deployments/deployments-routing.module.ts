import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DeploymentsComponent} from './component/deployments.component';
import {KeycloakAuthenticationGuard} from '../core/auth/keycloak-authentication-guard.service';

const routes: Routes = [{
  path: '',
  component: DeploymentsComponent,
  canActivate: [KeycloakAuthenticationGuard],
  data: {
    roles: ['ROLE_ADMIN'],
  },
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeploymentsRoutingModule { }
