import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';

const routes: Routes = [
  {
    path: 'deployments',
    loadChildren: './deployments/deployments.module#DeploymentsModule',
  },
  {path: '', redirectTo: 'deployments', pathMatch: 'prefix'},
  {path: '**', redirectTo: 'deployments'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
