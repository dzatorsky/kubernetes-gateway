# kubernetes-gateway

### Description
The project consists of 3 main applications:

1. front-end. (http://localhost:4200) Written in Angular, running on NodeJS for demo purposes.

2. back-end. (http://localhost:8080) Spring boot app with REST api

3. Keycloak. (http://keycloak:9080) Authorization server that provides OAuth 2.0 Security

### How to run the project locally
The easiest way to run everything would be using docker-compose setup.

##### Prerequisites
Before running the project you need to have the following preconditions met:

1. Running k8s cluster.

2. Have kubeconfig file with auth information for the running cluster.

##### Steps
1. Go to the `<project_dir>/docker`
2. `mkdir secrets` and copy your kubernetes config file inside this folder. The name of the file should be `kubeconfig`
3. `sudo /etc/hosts` we need to have the following loopback hosts there:

        127.0.0.1       keycloak
        127.0.0.1       back-end

    `keycloak` loopback is needed to make auth service working correctly inside docker-compose, otherwise token issuer will be different on gui and api.
    
    `back-end` loopback is needed to make it possible to setup everything without docker compose (NodeJS is using this host to proxy all the requests from GUI)

4. On root project dir: `docker-compose -f app.yml up`

5. Open http://localhost:4200

6. You will be redirected on auth server. `Username: admin, pass: admin`

7. After you entered the credentials you will be redirected back to http://localhost:4200 where you should see the GUI. 

Make sure you enter k8s namespace before proceeding with GUI operations.

### How to get test coverage report
1. Run `mvn test`
2. Grab the report in `target/site/jacoco/index.html`